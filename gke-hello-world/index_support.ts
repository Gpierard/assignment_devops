import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";
import * as docker from "@pulumi/docker";

export = async () => {
    const registry = gcp.container.getRegistryRepository();

    // await here is optional as pulumi is able to resolve promises itself
    const registryUrl = await registry.then(_r => _r.repositoryUrl);

    const imageName = "my-app";
    const appImage = new docker.Image(imageName, {
        imageName: pulumi.interpolate`${registryUrl}/${imageName}:v1.0.0`,
        build: {
            context: `./rstudio-server/`,
        },
    });

    return {
        registryUrl: (await registry).repositoryUrl,
        registryUrl2: registryUrl,
        imageName: pulumi.interpolate`${registryUrl}/${imageName}:v1.0.0`,
    }
}