import * as docker from "@pulumi/docker";
import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";
//import { config } from "./config";

const name = "helloworld";

// Create a GKE cluster
const engineVersion = gcp.container.getEngineVersions().then(v => v.latestMasterVersion);
const cluster = new gcp.container.Cluster(name, {
    initialNodeCount: 2,
    minMasterVersion: engineVersion,
    nodeVersion: engineVersion,
    nodeConfig: {
        machineType: "n1-standard-1",
        oauthScopes: [
            "https://www.googleapis.com/auth/compute",
            "https://www.googleapis.com/auth/devstorage.read_only",
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring"
        ],
    },
});

// Export the Cluster name
export const clusterName = cluster.name;

// Manufacture a GKE-style kubeconfig. Note that this is slightly "different"
// because of the way GKE requires gcloud to be in the picture for cluster
// authentication (rather than using the client cert/key directly).
export const kubeconfig = pulumi.
    all([ cluster.name, cluster.endpoint, cluster.masterAuth ]).
    apply(([ name, endpoint, masterAuth ]) => {
        const context = `${gcp.config.project}_${gcp.config.zone}_${name}`;
        return `apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${masterAuth.clusterCaCertificate}
    server: https://${endpoint}
  name: ${context}
contexts:
- context:
    cluster: ${context}
    user: ${context}
  name: ${context}
current-context: ${context}
kind: Config
preferences: {}
users:
- name: ${context}
  user:
    auth-provider:
      config:
        cmd-args: config config-helper --format=json
        cmd-path: gcloud
        expiry-key: '{.credential.token_expiry}'
        token-key: '{.credential.access_token}'
      name: gcp
`;
    });

// Create a Kubernetes provider instance that uses our cluster from above.
const clusterProvider = new k8s.Provider(name, {
    kubeconfig: kubeconfig,
});

// Create a Kubernetes Namespace
const ns = new k8s.core.v1.Namespace(name, {}, { provider: clusterProvider });

// Export the Namespace name
export const namespaceName = ns.metadata.apply(m => m.name);

// Create a NGINX Deployment
const appLabels = { appClass: name };
const deployment = new k8s.apps.v1.Deployment(name,
    {
        metadata: {
            namespace: namespaceName,
            labels: appLabels,
        },
        spec: {
            replicas: 1,
            selector: { matchLabels: appLabels },
            template: {
                metadata: {
                    labels: appLabels,
                },
                spec: {
                    containers: [
                        {
                            name: name,
                            image: "nginx:latest",
                            ports: [{ name: "http", containerPort: 80 }]
                        }
                    ],
                }
            }
        },
    },
    {
        provider: clusterProvider,
    }
);


// Export the Deployment name
export const deploymentName = deployment.metadata.apply(m => m.name);

// Create a LoadBalancer Service for the NGINX Deployment
const service = new k8s.core.v1.Service(name,
    {
        metadata: {
            labels: appLabels,
            namespace: namespaceName,
        },
        spec: {
            type: "LoadBalancer",
            ports: [{ port: 80, targetPort: "http" }],
            selector: appLabels,
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Service name and public LoadBalancer endpoint
export const serviceName = service.metadata.apply(m => m.name);
export const servicePublicIP = service.status.apply(s => s.loadBalancer.ingress[0].ip)






//################################ basic flask

const flaskname="myflask";
// Create a Kubernetes Namespace
const ns_flask = new k8s.core.v1.Namespace(flaskname, {}, { provider: clusterProvider });

// Export the Namespace name
export const namespaceName_flask = ns_flask.metadata.apply(m => m.name);
 // Create a NGINX basic Flask Deployment
const appLabels_flask = { appClass: flaskname };
const deployment_flask = new k8s.apps.v1.Deployment(flaskname,
    {
        metadata: {
            namespace: namespaceName_flask,
            labels: appLabels_flask,
        },
        spec: {
            replicas: 1,
            selector: { matchLabels: appLabels_flask },
            template: {
                metadata: {
                    labels: appLabels_flask,
                },
                spec: {
                    containers: [
                        {
                            name: flaskname,
                            image: "digitalocean/flask-helloworld:latest",
                            ports: [{ name: "http", containerPort: 5000 }]
                        }
                    ],
                }
            }
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Deployment name
export const deploymentName_flask = deployment_flask.metadata.apply(m => m.name);

// Create a LoadBalancer Service for the NGINX Deployment
const service_flask = new k8s.core.v1.Service(flaskname,
    {
        metadata: {
            labels: appLabels_flask,
            namespace: namespaceName_flask,
        },
        spec: {
            type: "LoadBalancer",
            ports: [{ port: 80, targetPort: "http" }],
            selector: appLabels_flask,
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Service name and public LoadBalancer endpoint
export const serviceName_flask = service_flask.metadata.apply(m => m.name);
export const servicePublicIP_flask = service_flask.status.apply(s => s.loadBalancer.ingress[0].ip)

// ---------------------########################
// TEST USING BASIC rstudio APP ##############################

const rsname="rstudio";
// Create a Kubernetes Namespace
//const ns_rs = new k8s.core.v1.Namespace(flaskname, {}, { provider: clusterProvider });

// Export the Namespace name
//export const namespaceName_flask = ns_flask.metadata.apply(m => m.name);
 // Create a NGINX basic Flask Deployment
const appLabels_rs = { appClass: rsname };

/* 
const customImagers = "myrstudio";
const mycustomimg = new docker.Image(customImagers, {
//    imageName: pulumi.interpolate`${registry.repositoryUrl}/${customImage}:v1.0.0`,
	imageName: customImagers,
//	imageName: "gcr.io/284308214775/myrstudio:latest",
//	imageName: "gcr.io/napoleongamesassignment/myrstudio:latest",
//	imageName: pulumi.interpolate`${gcrLocation}/${customImage}:v1.0.0`,
    build: {
        context: `./rstudio-server/`,
    },
	skipPush: true,
});
 */
const deployment_rs = new k8s.apps.v1.Deployment(rsname,
    {
        metadata: {
            namespace: namespaceName_flask,
            labels: appLabels_rs,
        },
        spec: {
            replicas: 1,
            selector: { matchLabels: appLabels_rs },
            template: {
                metadata: {
                    labels: appLabels_rs,
                },
                spec: {
                    containers: [
                        {
                            name: rsname,
                            image: "gpierard/myrepo",
                            ports: [{ name: "http", containerPort: 8787}]
                        }
                    ],
                }
            }
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Deployment name
export const deploymentName_rs = deployment_rs.metadata.apply(m => m.name);

// Create a LoadBalancer Service for the NGINX Deployment
const service_rs = new k8s.core.v1.Service(rsname,
    {
        metadata: {
            labels: appLabels_rs,
            namespace: namespaceName_flask,
        },
        spec: {
            type: "LoadBalancer",
            ports: [{ port: 80, targetPort: "http" }],
            selector: appLabels_rs,
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Service name and public LoadBalancer endpoint
export const serviceName_rs = service_rs.metadata.apply(m => m.name);
export const servicePublicIP_rs = service_rs.status.apply(s => s.loadBalancer.ingress[0].ip)




/* // ---------------------########################
// TEST USING BASIC rstudio APP ##############################

const rsname="rstudio";
// Create a Kubernetes Namespace
//const ns_rs = new k8s.core.v1.Namespace(flaskname, {}, { provider: clusterProvider });

// Export the Namespace name
//export const namespaceName_flask = ns_flask.metadata.apply(m => m.name);
 // Create a NGINX basic Flask Deployment
const appLabels_rs = { appClass: rsname };


const customImagers = "myrstudio";
const mycustomimg = new docker.Image(customImagers, {
//    imageName: pulumi.interpolate`${registry.repositoryUrl}/${customImage}:v1.0.0`,
	imageName: customImagers,
//	imageName: "gcr.io/284308214775/myrstudio:latest",
//	imageName: "gcr.io/napoleongamesassignment/myrstudio:latest",
//	imageName: pulumi.interpolate`${gcrLocation}/${customImage}:v1.0.0`,
    build: {
        context: `./rstudio-server/`,
    },
	skipPush: true,
});

const deployment_rs = new k8s.apps.v1.Deployment(rsname,
    {
        metadata: {
            namespace: namespaceName_flask,
            labels: appLabels_rs,
        },
        spec: {
            replicas: 1,
            selector: { matchLabels: appLabels_rs },
            template: {
                metadata: {
                    labels: appLabels_rs,
                },
                spec: {
                    containers: [
                        {
                            name: rsname,
                            image: "myrstudio",
                            ports: [{ name: "http", containerPort: 80}]
                        }
                    ],
                }
            }
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Deployment name
export const deploymentName_rs = deployment_rs.metadata.apply(m => m.name);

// Create a LoadBalancer Service for the NGINX Deployment
const service_rs = new k8s.core.v1.Service(rsname,
    {
        metadata: {
            labels: appLabels_rs,
            namespace: namespaceName_flask,
        },
        spec: {
            type: "LoadBalancer",
            ports: [{ port: 80, targetPort: "http" }],
            selector: appLabels_rs,
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Service name and public LoadBalancer endpoint
export const serviceName_rs = service_rs.metadata.apply(m => m.name);
export const servicePublicIP_rs = service_rs.status.apply(s => s.loadBalancer.ingress[0].ip)

 */

/* // Get the GCP project registry repository.
const registry = gcp.container.getRegistryRepository();
const gcrLocation = registry.then(registry => registry.repositoryUrl);
// Build a Docker image from a local Dockerfile context in the
// './mydockerimg' directory, and push it to the registry.
//const registry = gcp.container.getRegistryRepository();
const customImage = "mydockerimg";
const appImage = new docker.Image(customImage, {
//    imageName: pulumi.interpolate`${registry.repositoryUrl}/${customImage}:v1.0.0`,
//	imageName: "mydockerimg",
//	imageName: "gcr.io/284308214775/myrstudio:latest",
	imageName: "gcr.io/napoleongamesassignment/myrstudio:latest",
//	imageName: pulumi.interpolate`${gcrLocation}/${customImage}:v1.0.0`,
    build: {
        context: `./${customImage}/`,
    },
});
// Create a k8s provider.
// NOT NEEDED
// Create a Deployment of the built container.
const appLabels_helloworld = { app: customImage };
const appDeployment = new k8s.apps.v1.Deployment("app", {
    spec: {
        selector: { matchLabels: appLabels_helloworld },
        replicas: 1,
        template: {
            metadata: { labels: appLabels_helloworld },
            spec: {
                containers: [{
                    name: customImage,
                    image: appImage.imageName,
                    ports: [{name: "http", containerPort: 80}],
                }],
            }
        },
    }
}, { provider: clusterProvider });
export const appDeploymentName = appDeployment.metadata.apply(m => m.name);
 */