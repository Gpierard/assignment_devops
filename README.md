# Gauthier Pierard's assignment for the DevOps role

This documents the approach, questions and research done to fulfill this assignment.

---

## Original assignment

1. Create your code in a public bitbucket account
2. Use GCP to spin up a small Kubernetes cluster
3. Deploy a hello world container
4. Make the container public with an ingress controller towards the Kubernetes cluster. (You can chose technology or tool that you want. Istio, Anthos, Google Ingress, …)
Please do this with Pulumi (https://www.pulumi.com) based on TypeScript code.
You can use all the tools needed for this, with free accounts.

## Initial research 

Did some research to make sure I have an understanding about the key concepts.

 - From https://craft.co/pulumi:
    - 57 employees
    - 57.5M funding
    - started in 2017 out of Seattle
 - from https://www.pulumi.com/about/: Pulumi is a venture backed startup in Seattle whose mission is to enable every person to harness the power of the cloud.... our products sit uniquely at the intersection of developer tools and cloud operations.
 - From wikipedia: Infrastructure as code (IaC) is the process of managing and provisioning computer data centers through machine-readable definition files, rather than physical hardware configuration or interactive configuration tools.[1] The IT infrastructure managed by this process comprises both physical equipment, such as bare-metal servers, as well as virtual machines, and associated configuration resources. The definitions may be in a version control system. It can use either scripts or declarative definitions, rather than manual processes, but the term is more often used to promote declarative approaches.
    
- From https://www.pulumi.com/docs/intro/vs/chef-puppet-etc/: Chef, Puppet, Ansible, and Salt are all popular configuration management tools. These tools help you install and manage software on existing cloud infrastructure, either for bootstrapping a virtual machine, or patching one. They do not attempt to solve the problem of provisioning or updating infrastructure, containers, or serverless resources.
- Pulumi vs. Terraform: Terraform and Pulumi hold a lot of similarities, but they differ in a few key ways. This page helps provide a rundown of the differences. First, Pulumi is like Terraform, in that you create, deploy, and manage infrastructure as code on any cloud. **But where Terraform requires the use of a custom programming language, Pulumi allows you to use familiar general purpose languages and tools to accomplish the same goals**. Like Terraform, Pulumi is open source on GitHub and is free to use. Both Terraform and Pulumi support many cloud providers, including AWS, Azure, and Google Cloud, plus other services like CloudFlare, Digital Ocean, and more. Thanks to integration with Terraform providers, Pulumi is able to support a superset of the providers that Terraform currently offers.
- From wikipedia: TypeScript is a programming language developed and maintained by Microsoft. It is a strict syntactical superset of JavaScript and adds optional static typing to the language. TypeScript is designed for the development of large applications and transcompiles to JavaScript.[5] As TypeScript is a superset of JavaScript, existing JavaScript programs are also valid TypeScript programs.
- kubeconfig is a YAML file that contains either a username and password combination or a secure token that when read programmatically removes the need for the Kubernetes client to ask for interactive authentication. kubeconfig is the secure and standard method to enable access to your Kubernetes clusters.
- Service: An abstract way to expose an application running on a set of Pods as a network service.
With Kubernetes you don't need to modify your application to use an unfamiliar service discovery mechanism. Kubernetes gives Pods their own IP addresses and a single DNS name for a set of Pods, and can load-balance across them.
    - Motivation: Kubernetes Pods are created and destroyed to match the state of your cluster. Pods are nonpermanent resources. If you use a Deployment to run your app, it can create and destroy Pods dynamically.
Each Pod gets its own IP address, however in a Deployment, the set of Pods running in one moment in time could be different from the set of Pods running that application a moment later.
This leads to a problem: if some set of Pods (call them "backends") provides functionality to other Pods (call them "frontends") inside your cluster, how do the frontends find out and keep track of which IP address to connect to, so that the frontend can use the backend part of the workload? 
Enter Services (Service resources)
In Kubernetes, a Service is an abstraction which defines a logical set of Pods and a policy by which to access them (sometimes this pattern is called a micro-service). The set of Pods targeted by a Service is usually determined by a selector. To learn about other ways to define Service endpoints, see Services without selectors.
- A load balancer is a device that acts as a reverse proxy and distributes network or application traffic across a number of servers. Load balancers are used to increase capacity (concurrent users) and reliability of applications
- A proxy server, sometimes referred to as a forward proxy, is a server that routes traffic between client(s) and another system, usually external to the network. By doing so, it can regulate traffic according to preset policies, convert and mask client IP addresses, enforce security protocols, and block unknown traffic.
- A reverse proxy is a type of proxy server.  Unlike a traditional proxy server, which is used to protect clients, a reverse proxy is used to protect servers. A reverse proxy is a server that accepts a request from a client, forwards the request to another one of many other servers, and returns the results from the server that actually processed the request to the client as if the proxy server had processed the request itself. The client only communicates directly with the reverse proxy server and it does not know that some other server actually processed its request.
- The Kubernetes API
The core of Kubernetes' control plane is the API server. The API server exposes an HTTP API that lets end users, different parts of your cluster, and external components communicate with one another.
The Kubernetes API lets you query and manipulate the state of API objects in Kubernetes (for example: Pods, Namespaces, ConfigMaps, and Events).
Most operations can be performed through the kubectl command-line interface or other command-line tools, such as kubeadm, which in turn use the API. However, you can also access the API directly using REST calls.
Consider using one of the client libraries if you are writing an application using the Kubernetes API
- Namespaces:Kubernetes supports multiple virtual clusters backed by the same physical cluster. These virtual clusters are called namespaces
## Use GCP to spin up a small Kubernetes cluster

1. Created a free Bitbucket account with readme and .gitignore, cloned the repo locally on my Google Drive shared drive, on my home Windows 10 machine.
2. The general approach I will follow is https://www.pulumi.com/docs/tutorials/kubernetes/gke/
### Config steps
- Installed chocolatey using powershell
- Installed pulumi using chocolatey 
- Saved choco install script as .ps1 file under the "install scripts" folder
- Had to change ```set-executionpolicy remotesigned``` in order to be able to execute ps1 scripts.
- Installed Gcould SDK using the associated PS script
- Created NapoleonGamesAssignment project in Google Could in order to create a service account.
- Created service account in https://console.cloud.google.com/iam-admin
- Created JSON key
- Created envvar using gcloud_serviceaccountsetup.ps1
- Running the pulumi_new ps1 script
    - **ERROR** npm ERR! EEXIST: file already exists, mkdir 'G:\My Drive\repos\assignment_devops\gke-hello-world\node_modules\.staging'
    - This was fixed when moving to C:, there is probably a *staging* folder used by Google drive. 
- Installed the required npm packages using ```npm install --save @pulumi/pulumi @pulumi/gcp @pulumi/kubernetes```
### Step 1: Create new GKE cluster

```typescript
import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";

const name = "helloworld";

// Create a GKE cluster
const engineVersion = gcp.container.getEngineVersions().then(v => v.latestMasterVersion);
const cluster = new gcp.container.Cluster(name, {
    initialNodeCount: 2,
    minMasterVersion: engineVersion,
    nodeVersion: engineVersion,
    nodeConfig: {
        machineType: "n1-standard-1",
        oauthScopes: [
            "https://www.googleapis.com/auth/compute",
            "https://www.googleapis.com/auth/devstorage.read_only",
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring"
        ],
    },
});

// Export the Cluster name
export const clusterName = cluster.name;

// Manufacture a GKE-style kubeconfig. Note that this is slightly "different"
// because of the way GKE requires gcloud to be in the picture for cluster
// authentication (rather than using the client cert/key directly).
export const kubeconfig = pulumi.
    all([ cluster.name, cluster.endpoint, cluster.masterAuth ]).
    apply(([ name, endpoint, masterAuth ]) => {
        const context = `${gcp.config.project}_${gcp.config.zone}_${name}`;
        return `apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${masterAuth.clusterCaCertificate}
    server: https://${endpoint}
  name: ${context}
contexts:
- context:
    cluster: ${context}
    user: ${context}
  name: ${context}
current-context: ${context}
kind: Config
preferences: {}
users:
- name: ${context}
  user:
    auth-provider:
      config:
        cmd-args: config config-helper --format=json
        cmd-path: gcloud
        expiry-key: '{.credential.token_expiry}'
        token-key: '{.credential.access_token}'
      name: gcp
`;
    });

// Create a Kubernetes provider instance that uses our cluster from above.
const clusterProvider = new k8s.Provider(name, {
    kubeconfig: kubeconfig,
});
```
### Step 2: Access and modify the cluster using Pulumi Providers: creating an Nginx LB deployment

```
// Create a Kubernetes Namespace
const ns = new k8s.core.v1.Namespace(name, {}, { provider: clusterProvider });

// Export the Namespace name
export const namespaceName = ns.metadata.apply(m => m.name);

// Create a NGINX Deployment
const appLabels = { appClass: name };
const deployment = new k8s.apps.v1.Deployment(name,
    {
        metadata: {
            namespace: namespaceName,
            labels: appLabels,
        },
        spec: {
            replicas: 1,
            selector: { matchLabels: appLabels },
            template: {
                metadata: {
                    labels: appLabels,
                },
                spec: {
                    containers: [
                        {
                            name: name,
                            image: "nginx:latest",
                            ports: [{ name: "http", containerPort: 80 }]
                        }
                    ],
                }
            }
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Deployment name
export const deploymentName = deployment.metadata.apply(m => m.name);

// Create a LoadBalancer Service for the NGINX Deployment
const service = new k8s.core.v1.Service(name,
    {
        metadata: {
            labels: appLabels,
            namespace: namespaceName,
        },
        spec: {
            type: "LoadBalancer",
            ports: [{ port: 80, targetPort: "http" }],
            selector: appLabels,
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Service name and public LoadBalancer endpoint
export const serviceName = service.metadata.apply(m => m.name);
export const servicePublicIP = service.status.apply(s => s.loadBalancer.ingress[0].ip)
 
```

## Step 3: Customizing

I am going to create my own deployment using a basic flask - hello world docker image.
```
// STEP 3 : CREATING A DEPLOYMENT USING A BASIC FLASK APP ##############################
const flaskname="basicflask";
// Create a Kubernetes Namespace
const ns_flask = new k8s.core.v1.Namespace(flaskname, {}, { provider: clusterProvider });
// Export the Namespace name
export const namespaceName_flask = ns_flask.metadata.apply(m => m.name);

 // Create a NGINX basic Flask Deployment
const appLabels_flask = { appClass: flaskname };
const deployment_flask = new k8s.apps.v1.Deployment(flaskname,
    {
        metadata: {
            namespace: namespaceName_flask,
            labels: appLabels_flask,
        },
        spec: {
            replicas: 1,
            selector: { matchLabels: appLabels_flask },
            template: {
                metadata: {
                    labels: appLabels_flask,
                },
                spec: {
                    containers: [
                        {
                            name: flaskname,
                            image: "digitalocean/flask-helloworld:latest",
                            ports: [{ name: "http", containerPort: 5000 }]
                        }
                    ],
                }
            }
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Deployment name
export const deploymentName_flask = deployment_flask.metadata.apply(m => m.name);
// Create a LoadBalancer Service for the NGINX Deployment
const service_flask = new k8s.core.v1.Service(flaskname,
    {
        metadata: {
            labels: appLabels_flask,
            namespace: namespaceName_flask,
        },
        spec: {
            type: "LoadBalancer",
            ports: [{ port: 80, targetPort: "http" }],
            selector: appLabels_flask,
        },
    },
    {
        provider: clusterProvider,
    }
);

// Export the Service name and public LoadBalancer endpoint
export const serviceName_flask = service_flask.metadata.apply(m => m.name);
export const servicePublicIP_flask = service_flask.status.apply(s => s.loadBalancer.ingress[0].ip)
```

### Notes:
- had to map the container port to 5000 (still exposing externally to port 80)
- Changed a few variable names including the names ```flaskname="basicflask";``` and replacing in subsequent fields
- Cleaned up afterwards using ```pulumi destroy```

## Conclusion: bringing it all together
By replacing the variable names in the index.ts script, I was able to expose both services publicly, the LB as well as the flask app.


![results](img/results.pgn)

In conclusion, I think that Pulumi is quite a powerful tool, using infrastructure as code to create a greater degree of abstraction with respect to infrastructure, clusters, and services, by at the same time allowing the use of several languages (TypeScript, JavaScript, Python, Go, C#).

